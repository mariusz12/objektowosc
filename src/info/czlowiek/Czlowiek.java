package info.czlowiek;

public class Czlowiek {

    private String imie;
    private String nazwisko;

    public String getImie() {
        return imie;
    }

    public void setImie(final String imie) {
        this.imie = imie;
    }

    public String getNazwisko() {
        return nazwisko;
    }

    public void setNazwisko(final String nazwisko) {
        this.nazwisko = nazwisko;
    }
}
