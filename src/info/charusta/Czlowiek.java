package info.charusta;/*
public
private
protected

 */

public class Czlowiek {

    private String imie;
    private String nazwisko;
    private int wzrost;
    private short wiek;

    public Czlowiek() {
    }

    public Czlowiek(String imie, String nazwisko, int wzrost) {
        this.imie = imie;
        this.nazwisko = nazwisko;
        this.wzrost = wzrost;
    }

    public Czlowiek(String imieParametr, String nazwisko, int wzrost, short wiek) {
        this.imie = imieParametr;//tutaj jest to parametr
        this.nazwisko = nazwisko;//czarne nazwisko to tez parametr, bordowe to pole klasy
        this.wzrost = wzrost;
        this.wiek = wiek;
    }

    public String toString() {
        return "imie='" + imie + '\'' +
               ", nazwisko='" + nazwisko + '\'' +
               ", wzrost=" + wzrost +
               ", wiek=" + wiek
            ;
    }

    protected String getImie() {
        return imie;
    }

    public void setImie(final String imie) {
        this.imie = imie;
    }

    public void nieWykorzystanaMetoda(){
        System.out.println(2+2);
    }

    public String getNazwisko() {
        return nazwisko;
    }

    public void setNazwisko(final String nazwisko) {
        this.nazwisko = nazwisko;
    }

    public int getWzrost() {
        return wzrost;
    }

    public void setWzrost(final int wzrost) {
        this.wzrost = wzrost;
    }

    public short getWiek() {
        return wiek;
    }

    public void setWiek(final short wiek) {
        this.wiek = wiek;
    }

}
