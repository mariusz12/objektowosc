package info.charusta;



public class Main {

    public static void main(String[] args) {
        Czlowiek czlowiek1 = new Czlowiek();
        czlowiek1.setImie("Mariusz");
        czlowiek1.setNazwisko("Charusta");
        czlowiek1.setWiek((short) 18);
        czlowiek1.setWzrost(178);

        String czlowiekImie = czlowiek1.getImie();
        String czlowiekNazwisko = czlowiek1.getNazwisko();
        int czlowiekWzrost= czlowiek1.getWzrost();
        short czlowiekWiek = czlowiek1.getWiek();

        System.out.println(czlowiek1.toString());
        System.out.printf("%s %s %s %s",czlowiekImie, czlowiekNazwisko, czlowiekWiek, czlowiekWzrost);

        System.out.println();
        czlowiek1.setImie("Marcin");
        System.out.println(czlowiek1.getImie());

        Czlowiek czlowiek2 = new Czlowiek();
        czlowiek2.setImie("Dariusz");
        czlowiek2.setNazwisko("Kowalski");
        czlowiek2.setWiek((short) 30);
        czlowiek2.setWzrost(210);


        System.out.println();
        czlowiekImie = czlowiek2.getImie();
        czlowiekNazwisko = czlowiek2.getNazwisko();
        czlowiekWzrost= czlowiek2.getWzrost();
        czlowiekWiek = czlowiek2.getWiek();
        System.out.printf("%s %s %s %s",czlowiekImie, czlowiekNazwisko, czlowiekWiek, czlowiekWzrost);

        System.out.println();

        Czlowiek czlowiek3 = new Czlowiek("Maria", "Konopnicka", 160, (short) 45);
        System.out.println(czlowiek3.toString());

        System.out.println();

        Czlowiek czlowiek4 = new Czlowiek("Tadeusz", "Kościuszko", 180);
        System.out.println(czlowiek4.toString());


        Czlowiek czlowiek5 =new Czlowiek();
        czlowiek5.setImie("Janek");

        System.out.println(czlowiek5.toString());

    }

}
